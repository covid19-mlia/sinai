# -------------------------------------------------------------------------------------------------------------------------------------------------------- #
#   Required
# -------------------------------------------------------------------------------------------------------------------------------------------------------- #

#   Libraries
from elasticsearch import helpers
from elasticsearch import Elasticsearch
import pandas as pd
import time
import re

#   Config Parameters
INDEX = 'mlia'
UPLOAD_FORMAT = 'sinai_task2_round1_mono-es_bm25.txt'

# -------------------------------------------------------------------------------------------------------------------------------------------------------- #
#   Elasticsearch
# -------------------------------------------------------------------------------------------------------------------------------------------------------- #
class Elastic:

    # Elasticsearch instance
    es = None

    # Elasticsearch config
    hostname = "127.0.0.1:9200"
    type = "doc_es"

    # Initialization to declared hostname
    def __init__(self):
        self.es = Elasticsearch(self.hostname, timeout=50)

    # Retrieve documents from elasticsearch based on the concept More Like This 
    # https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-mlt-query.html
    # query: Textual query
    def getDocuments(self, topic_query, searchByTitle = False, searchByKeyword = False, searchByContent = False, size = 1000):
        
        response = False

        # Query format
        body = { 'query': {} }

        if topic_query:

            # Elasticsearch Query Format transformation

            body['query'] = {
                'more_like_this': {
                    'fields' : [],
                    'like' : topic_query,
                    'min_term_freq' : 1,
                    'max_query_terms' : 20,
                }
            }

            # Restrict query
            if not searchByTitle and not searchByKeyword and not searchByContent:
                raise Exception('Not field provided to perform document retrieval')

            if searchByTitle:
                body['query']['more_like_this']['fields'].append('title')

            if searchByKeyword:
                body['query']['more_like_this']['fields'].append('keyword')

            if searchByContent:
                body['query']['more_like_this']['fields'].append('content')

            # Call Elasticsearch and format given response
            try:
                
                response = []
                returned = self.es.search(body=body,index=INDEX, size=size)
                for h in returned['hits']['hits']:
                    item = { 'score': h['_score'], 'id': h['_id'], 'title': h['_source']['title'], 'keywords': h['_source']['keyword'] }
                    response.append(item)

            except Exception:
                pass    

        return response
# -------------------------------------------------------------------------------------------------------------------------------------------------------- #
#   TREC
# -------------------------------------------------------------------------------------------------------------------------------------------------------- #

class TREC:

    # Trec Parameters
    topic = None
    query = 'Q0'
    docID = None
    rank = (-1)
    score = 0
    run = None

    # Initialization
    def __init__(self, topic, docID, rank, score, run):
        self.topic = topic
        self.docID = docID
        self.rank = rank
        self.score = score
        self.run = run

    # Format return
    def getTrecFormat(self):
        return {0: self.topic, 1: self.query, 2010: self.docID, 3: self.rank, 4: self.score, 5: self.run}   

# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
#   Execution section
# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
if __name__ == "__main__":

    start_time = time.time()

    # Run assignment
    TOPICS_FILE = 'D:\\MLIA\\TASK 2\\Runs\\topics_task_2_round_1_es_formatted.tsv'
    DESTINATION_PATH = 'D:\\MLIA\\TASK 2\\Runs\\Subtask2\\'+UPLOAD_FORMAT
    run = 'sinai4'
    size = 200

    # Search Filters
    searchByTitle = True
    searchByKeyword = True
    searchByContent = True

    # Topic argument search
    searchByKeyword = True
    searchByConversational = True
    searchByExplanation = True

    try:

        # (1) Read Topics File
        df = pd.read_csv(TOPICS_FILE, delimiter='\t' , engine='python', encoding='utf-8')
        topics = df.to_dict()

        response = []
        for i in range(0,len(topics['topic'])):

           # 'topic': topics['topic'][i], 'keyword': topics['keyword'][i] , 'conversational': topics['conversational'][i], 'explanation': topics['explanation'][i]
           
            try:
                # (2) Perform search
                es = Elastic()
                topic_query = ''

                if searchByKeyword:
                    topic_query+=topics['keyword'][i]+' '

                if searchByConversational:
                    topic_query+=topics['conversational'][i]+' '

                if searchByExplanation:
                    topic_query+=topics['explanation'][i]+' '
                    
                if not searchByKeyword and not searchByConversational and not searchByExplanation:
                    raise Exception('Not provided argument to search by.')

                query = es.getDocuments(topic_query=topic_query, searchByTitle=searchByTitle, searchByKeyword=searchByKeyword, searchByContent=searchByContent, size = size)

                # (3) Format to TREC
                for index, r in enumerate(query):
                    trec = TREC(topic = topics['topic'][i], docID = r['id'].replace('.xml',''), rank = index, score = r['score'], run = run )
                    response.append(trec.getTrecFormat())

            except Exception as e:
                print(e)

        # (3) PRINTING RESULTS TO EVALUATION FORMAT
        df = pd.DataFrame(response)
        df.to_csv(DESTINATION_PATH, sep = ' ', encoding='utf-8', index=False, header = False)

        print('Finished retrieving results ---- Check %s ' % DESTINATION_PATH)

    except Exception as e:
        print(e)

    print("--- %s seconds ---" % (time.time() - start_time))

# -----------------------------------------------------------------------------------------------------------------------------------------END--OF--FILE--  #