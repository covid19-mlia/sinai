#   File: 
#       indexXML.py  
#   
#   Description: 
#       The following code has been developed in order to extract textual information from XML files and insert them into elasticsearch once formatted.
#       
#   Content:
#       (1) Required libraries
#       (2) Elasticsearch
#       (3) XML
#       (4) Execute code

# -------------------------------------------------------------------------------------------------------------------------------------------------------- #
#   Required
# -------------------------------------------------------------------------------------------------------------------------------------------------------- #

#   Libraries
from elasticsearch import helpers
from elasticsearch import Elasticsearch
from bs4 import BeautifulSoup as bs
from datetime import datetime
import multiprocessing as mp
import time
import os
import re

#   Config Parameters
CORPUS_PATH = 'D:\\MLIA\\TASK 2\\Corpus\\'
F_DIR = 'medisys-202004-xml_ir'
INDEX = 'mlia'
N_CORES = 7

# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
#   Elasticsearch dedicated section 
# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
class Elastic:

    # Elasticsearch instance
    es = None

    # Elasticsearch config
    hostname = "127.0.0.1:9200"
    type = "doc_es"

    # Initialization to declared hostname
    def __init__(self):
        self.es = Elasticsearch(self.hostname)

    # Generates format for multiple documents
    # d_list: document list containing its corresponding data
    def gendata(self, d_list):
        for doc in d_list:
            yield {
                "_index": INDEX,
                '_id': doc['docID'],
                "_source": { 'title':  doc['title'], 'keyword':  doc['keyword'], 'content':  doc['content'] },
            }

    # Inserts multiple documents into elasticsearch index
    def multipleInsert(self, d_list):

        try:
            helpers.bulk(self.es, self.gendata(d_list))
            print('Inserted documents into index: %s' % (INDEX))
        except Exception:
            print("Couldn't insert documents into index: %s" % (INDEX))

# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
#   XML abstraction dedicated section
# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
class XMLDOC:        

    # Get all values inside soup with given tag
    def getByTag(self, soup, tag):

        content = ''
        try:
            content = soup.find_all(tag)        
        except Exception:
            pass

        return content

    # Transforms into specific format
    def formatting(self, path, xmlContent):
        
        response = None

        try:

            soup = bs(xmlContent,'xml')

            # Get document ID
            p_slices = path.split('\\')
            docID = p_slices[len(p_slices)-1]

            # Get document Title
            title = self.getByTag(soup,'title')
            if len(title) > 0:
                title = title[0].text

            # Get document Keywords
            keyterms = self.getByTag(soup,'keyTerm')
            keyword = ''
            if len(keyterms) > 0:                
                for index, term in enumerate(keyterms): 
                    keyword+=term.text
                    if index+1 < len(keyterms):
                        keyword+=','

            # Get document content
            body = soup.find_all(name='p')
            content = ''
            if len(body) > 0:
                for index, term in enumerate(body): 
                    if not term.has_attr('crawlinfo'):
                        content+=term.text
                        if index+1 < len(content):
                            content+=' '

            response = {'docID': docID, 'title': title, 'keyword': keyword, 'content': content }

        except Exception:
            pass

        return response        
        
    # Retrieve XML content 
    def getXMLContent(self, path):

        try:
            with open(path, "r",  encoding='utf-8',  errors='replace') as file:
                # Read each line in the file, readlines() returns a list of lines
                content = file.readlines()
                # Combine the lines in the list into a string
                content = "".join(content)
                # Give format
                xmlContent = self.formatting(path, content)

        except Exception:
            return {'docID': '', 'docData': {'title': '', 'keyword': '', 'content': ''} }

        return xmlContent     

# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
#   Execution section
# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
if __name__ == "__main__":

    start_time = time.time()

    # Config
    s_path = CORPUS_PATH+F_DIR+'\\'+'medisys_2020_04_4_res_xml-es'

    try:

        # (1) Retrieve all documents list
        f_list = os.listdir(s_path)
        for index, f in enumerate(f_list): 
            f_list[index]=(s_path+'\\'+f)
        print('Number of files: %s' % len(f_list))

        # (2) Concurrent XML Data Extraction
        xml = XMLDOC()
        p = mp.Pool(N_CORES) 
        documents = p.map(xml.getXMLContent, f_list)
        p.close()
        p.join()    

        # (3) Data insertion into Elasticsearch index
        es = Elastic()
        es.multipleInsert(documents)        

    except Exception:
        print('Could not perform request.')    

    print("--- %s seconds ---" % (time.time() - start_time))

# -----------------------------------------------------------------------------------------------------------------------------------------END--OF--FILE--  #

    

    