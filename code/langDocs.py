# -------------------------------------------------------------------------------------------------------------------------------------------------------- #
#   Required
# -------------------------------------------------------------------------------------------------------------------------------------------------------- #
 
#   Libraries
import pandas as pd
import shutil
import time
import os

#   Config Parameters
SEPARATOR = '-'
EXTRACTION_LANG = SEPARATOR+'es'
CORPUS_PATH = 'D:\\MLIA\\TASK 2\\Corpus\\'
F_PATH = 'medisys-202004-xml_ir\\medisys_2020_04_2_res_xml'

# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
#   Language extractor
# --------------------------------------------------------------------------------------------------------------------------------------------------------  #

# Document extraction to different directory
def replicateCorpus(path):

  #   (1) Retrieve all files names
  p_files = os.listdir(path)

  #   (2) Use pandas to find only those with the chosen language extension
  df = pd.DataFrame(p_files)
  df1 = df[df[0].str.contains(EXTRACTION_LANG)]

  #   (3) Validate target folder existance
  t_path = path+'-es'
  if not os.path.exists(t_path):  os.mkdir(t_path)
  
  #   (4) Replication of original file in the new folder
  replicated = 0
  for f in df1.values.tolist(): 
    replicated+=1
    shutil.copyfile( (path+'\\'+f[0]) , ((t_path)+'\\'+f[0]) )

  return replicated


# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
#   Execution section
# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
if __name__ == "__main__":
  start_time = time.time()

  try:
    print('Replicated documents: %s' % (replicateCorpus(CORPUS_PATH+F_PATH)) )
  except Exception as e:
    print(e)

  print("--- %s seconds ---" % (time.time() - start_time))
# -----------------------------------------------------------------------------------------------------------------------------------------END--OF--FILE--  #