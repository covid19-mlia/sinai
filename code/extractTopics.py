# -------------------------------------------------------------------------------------------------------------------------------------------------------- #
#   Required
# -------------------------------------------------------------------------------------------------------------------------------------------------------- #
 
#   Libraries
from bs4 import BeautifulSoup as bs
import pandas as pd
import shutil
import time
import os

#   Config Parameters
TASK_PATH = 'D:\\MLIA\\TASK 2\\'

# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
#   XML abstraction dedicated section
# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
class XMLDOC:        

    # Get all values inside soup with given tag
    def getByTag(self, soup, tag):

        content = ''
        try:
            content = soup.find_all(tag)        
        except Exception:
            pass

        return content

    # Transforms into specific format
    def formatting(self, path, xmlContent):
        
        response = None

        try:

            response = []
            soup = bs(xmlContent,'xml')

            # Get topic
            topic = self.getByTag(soup,'topic')

            for t in topic:

                # Get topic number
                number = int(t.attrs['number'])
                
                # Get keyword
                keyword = t.find_all('keyword')[0].text.strip()

                # Get conversational
                conversational = t.find_all('conversational')[0].text.strip()

                # Get explanation
                explanation = t.find_all('explanation')[0].text.strip()

                response.append({ 'topic': number, 'keyword': keyword , 'conversational': conversational, 'explanation': explanation})

        except Exception:
            pass

        return response        
        
    # Retrieve XML content 
    def getXMLContent(self, path):

        try:
            with open(path, "r",  encoding='utf-8',  errors='replace') as file:
                # Read each line in the file, readlines() returns a list of lines
                content = file.readlines()
                # Combine the lines in the list into a string
                content = "".join(content)
                # Give format
                xmlContent = self.formatting(path, content)

        except Exception:
            return False

        return xmlContent     

# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
#   Topic extractor
# --------------------------------------------------------------------------------------------------------------------------------------------------------  #
if __name__ == "__main__":

    start_time = time.time()

    # Relative Paths
    ORIGIN_PATH = TASK_PATH + 'topics_task_2_round_1_es.xml'
    DESTINATION_PATH = TASK_PATH+'Runs\\topics_task_2_round_1_es_formatted.tsv'

    try:

        # (1) Get XML Document all data formatted
        xml = XMLDOC()
        topics = xml.getXMLContent(ORIGIN_PATH)

        # (2) Store in Pandas TSV Format
        df = pd.DataFrame(topics)
        df.to_csv(DESTINATION_PATH, sep = '\t', encoding='utf-8')

        print('--- Executed successfully')

    except Exception:
        print('--- Error trying to format XML document')

    print("--- %s seconds ---" % (time.time() - start_time))
 